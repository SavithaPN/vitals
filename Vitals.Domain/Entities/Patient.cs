﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Domain.Interfaces;

namespace Vitals.Domain.Entities
{
    public class Patient : IBasicTrackable
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public Guid ExternalId { get; set; }
        [Required]
        public Guid SubscriptionId { get; set; }
        public DateTime CreatedDate { get ; set ; }
        public Guid CreatedBy { get; set ; }
        public DateTime LastAction { get ; set; }
        public Guid LastActionBy { get ; set ; }
    }
}
