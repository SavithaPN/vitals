﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Domain.Interfaces;

namespace Vitals.Domain.Entities
{
    public class Subscription :IBasicTrackable
    {
        public Guid Id { get; set; }
        public string AgentName { get; set; }
        public Guid CustomerId { get; set; }
        public DateTime CreatedDate { get ; set ; }
        public Guid CreatedBy { get; set ; }
        public DateTime LastAction { get ; set; }
        public Guid LastActionBy { get ; set ; }
    }
}
