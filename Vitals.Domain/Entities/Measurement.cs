﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vitals.Domain.Entities
{
    public class Measurement
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Patient Patient { get; set; }
        public Guid MeasurementTypeId { get; set; }
        public MeasurementType MeasurementType { get; set; }
        public Guid LinkedMeasurementId { get; set; }
        public string MeasurementValueTypeId { get; set; }
        public string Value { get; set; }
        public DateTime MeasurementDateTime { get; set; }
        public DateTime ObserverdDateTime { get; set; }
        public bool IsManual { get; set; }
        public string TakenBy { get; set; }
        public string TakenAt { get; set; }
        public bool IsProcessed { get; set; }
        public string AlertType { get; set; }
        public string AlertString { get; set; }
       
    }
}
