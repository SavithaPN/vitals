﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Domain.Interfaces;

namespace Vitals.Domain.Entities
{
    public class Customer: IBasicTrackable
    {
        public Guid Id { get; set; }
        public string CustomerName { get; set; }
        public string Group { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Address { get; set; }
        public string DateTimeFormat { get; set; }
        public bool Active { get; set; }

        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
        
    }
}
