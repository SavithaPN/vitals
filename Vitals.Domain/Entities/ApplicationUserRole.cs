﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Domain.Interfaces;

namespace Vitals.Domain.Entities
{
    public class ApplicationUserRole : IdentityRole, IBasicTrackable
    {
        public ApplicationUserRole() : base() { }
        public ApplicationUserRole(string role): base(role) { }

        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }

    }
}
