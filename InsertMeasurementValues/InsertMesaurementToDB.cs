using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;

namespace InsertMeasurementValues
{
    public static class InsertMesaurementToDB
    {

        private static string storageAccount = "storageaccountvital87bc";
        private static string storageKey = "RYC3zSosMTTIbycUXSsfMh9B0wGV0cVEpyguqwQtOE8d1CxzqR9pyx7iuRIGmZFiN/xt7WX+obVWE2NwA2SMew==";

        [FunctionName("InsertMesaurementToDB")]
        public static void Run([QueueTrigger("measurement-values-queue", Connection = "QueueStorage")]MeasurementDto myQueueItem, TraceWriter log, ExecutionContext context)
        {
            //log.Info($"C# Queue trigger function processed: {myQueueItem}");
            AddMeasurementToDB(myQueueItem, context.FunctionAppDirectory);
        }

        private static void AddMeasurementToDB(MeasurementDto myQueueItem, string path)
        {
            try
            {
                var config = new ConfigurationBuilder().SetBasePath(path)
                   .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                   .AddEnvironmentVariables().Build();

                var client = new HttpClient();
                client.BaseAddress = new Uri(config["ApiLayer"]);

                var grant_type = "password";
                var username = "admin";
                var password = "admin123";

                var formContent = new FormUrlEncodedContent(new[]
                {
                new KeyValuePair<string, string>("grant_type", grant_type),
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password)
            });

                var tokenObject = client.PostAsync("Token", formContent).Result;
                var token = tokenObject.Content.ReadAsStringAsync().Result;
                var authObject = tokenObject.Content.ReadAsAsync<AuthResponseDto>().Result;

                //Make the second call here to api/Measurement/AddMeasurements
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Authorization", "Bearer " + authObject.access_token);
                var output = client.GetAsync("/Measurement/AddMeasurements").Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                var storageAccountInstance = new CloudStorageAccount(new StorageCredentials(storageAccount, storageKey), true);
                var client = storageAccountInstance.CreateCloudQueueClient();
                var queue = client.GetQueueReference("measurement-values-queue");
            }
        }
    }

    public class AuthResponseDto
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string userName { get; set; }
    }
}
