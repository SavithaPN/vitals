﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertMeasurementValues
{
    public class MeasurementTypeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public decimal StartRange { get; set; }
        public decimal EndRange { get; set; }
    }
}
