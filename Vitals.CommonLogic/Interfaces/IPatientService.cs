﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.Domain.Entities;

namespace Vitals.CommonLogic.Interfaces
{
    public interface IPatientService
    {
        Task<IEnumerable<Patient>> ListPatientsBasedOnCustId(Guid customerId);
        Task<IEnumerable<Patient>> ListAllPatients();
        Task<Patient> GetPatientByExternalId(string sExternalId);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddPatient(Patient patient);
    }
}
