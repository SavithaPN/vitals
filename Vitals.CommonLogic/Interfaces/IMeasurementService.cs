﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.Common.Dto.Filters;
using Vitals.Common.Dto.PagedList;
using Vitals.Domain.Entities;

namespace Vitals.CommonLogic.Interfaces
{
    public interface IMeasurementService
    {
        Task<IEnumerable<MeasurementType>> GetMeasurementTypes();
        Task<IEnumerable<MeasurementValueType>> GetMeasurementValueTypes();
        Task<IEnumerable<Measurement>> GetMeasurements(Guid patientId, Guid subscriptionId, DateTime from, DateTime to);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddMeasurement(Measurement measurement);
        Task<PagedListViewModel<Measurement>> GetPagedMeasurements(Guid patientId, Guid subscriptionId, DateTime from, DateTime to, int skip =-1, int count=-1);
    }
}
