﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Domain.Entities;

namespace Vitals.CommonLogic.Interfaces
{
    public interface IApplicationUserService
    {
        Task<IdentityResult> RegisterUser(ApplicationUser user, string password);
        Task<IdentityResult> UpdateUser(ApplicationUser user);
        Task<ApplicationUser> FindUser(string userName, string password);
        Task<ApplicationUser> FindUserById(string id);
        List<ApplicationUser> GetUsers();

        Task<IdentityResult> AddRole(ApplicationUserRole userRole);
        Task<IdentityResult> EditRole(ApplicationUserRole userRole);
        List<ApplicationUserRole> GetRoles();
    }
}
