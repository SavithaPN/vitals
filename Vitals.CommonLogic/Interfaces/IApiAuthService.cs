﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;

namespace Vitals.CommonLogic.Interfaces
{
    public interface IApiAuthService
    {
        AuthResponseDto Authenticate(AuthInformationDto user);
    }
}
