﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.Domain.Entities;

namespace Vitals.CommonLogic.Interfaces
{
    public interface ICustomerService
    {
        Task<IEnumerable<Customer>> ListCustomers();
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddCustomer(Customer customer);
    }
}
