﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Domain.Entities;

namespace Vitals.CommonLogic.Interfaces
{
    public interface IVitalsService
    {
        //Task<IEnumerable<Vitals>> ListVitals();
        Task<IEnumerable<VitalType>> ListVitalTypes();
    }
}
