﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.CommonLogic.Interfaces;
using Vitals.DataAccess.Repository.Interfaces;
using Vitals.Domain.Entities;

namespace Vitals.CommonLogic.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CustomerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _customerRepository = _unitOfWork.GetRepository<Customer>();
        }

        public Task<IEnumerable<Customer>> ListCustomers()
        {
            return _customerRepository.GetAsync();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddCustomer(Customer customer)
        {
            try
            {
                _customerRepository.Insert(customer);
                await _unitOfWork.SaveAsync();
                return new WorkResultDto<Guid, GeneralWorkStatus>(customer.Id, GeneralWorkStatus.Success);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(ex.Message);
                return result;
            }
        }
    }
}
