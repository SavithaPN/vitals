﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.CommonLogic.Interfaces;
using Vitals.DataAccess.Repository.Interfaces;
using Vitals.Domain.Entities;

namespace Vitals.CommonLogic.Services
{
    public class PatientService : IPatientService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Patient> _patientRepository;
        private readonly IRepository<PatientExternalId> _patientExternalIdRepository;
        private readonly IRepository<Subscription> _subcriptionRepository;
        public PatientService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _patientExternalIdRepository = unitOfWork.GetRepository<PatientExternalId>();
            _patientRepository = unitOfWork.GetRepository<Patient>();
            _subcriptionRepository = unitOfWork.GetRepository<Subscription>();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddPatient(Patient patient)
        {
            try
            {
                //Check if the subscription id is valid
                var result = await _subcriptionRepository.GetAsync(s => s.Id == patient.SubscriptionId);
                if (result != null && result.Count() > 0)
                {
                    _patientRepository.Insert(patient);
                    await _unitOfWork.SaveAsync();
                    return new WorkResultDto<Guid, GeneralWorkStatus>(patient.Id, GeneralWorkStatus.Success);
                }
                else
                {
                    WorkResultDto<Guid, GeneralWorkStatus> resultData = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                    resultData.Errors.Add("Invalid Subscription id supplied");
                    return resultData;
                }
            }
            catch (Exception ex)
            {
                WorkResultDto<Guid, GeneralWorkStatus> result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(ex.Message);
                return result;
            }

        }

        public async Task<Patient> GetPatientByExternalId(string sExternalId)
        {
            var patients = await _patientRepository.GetAsync(p => p.ExternalId == Guid.Parse(sExternalId));
            return patients.FirstOrDefault();
        }

        public async Task<IEnumerable<Patient>> ListAllPatients()
        {
            var patients = await _patientRepository.GetAsync();
            return patients;
        }

        public async Task<IEnumerable<Patient>> ListPatientsBasedOnCustId(Guid customerId)
        {
            var patients = await _patientRepository.GetAsync(p => p.CustomerId == customerId);
            return patients;
        }
    }
}
