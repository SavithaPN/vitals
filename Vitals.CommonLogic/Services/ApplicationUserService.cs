﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Vitals.CommonLogic.Interfaces;
using Vitals.DataAccess.Contexts;
using Vitals.DataAccess.Repository.Interfaces;
using Vitals.Domain.Entities;

namespace Vitals.CommonLogic.Services
{
    public class ApplicationUserService : IApplicationUserService
    {
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<ApplicationUserRole> _roleManager;
        private IUnitOfWork _unitOfowrk;
        private ApplicationIdentityDbContext _context;

        public ApplicationUserService(IUnitOfWork unitOfwork)
        {
            _context = new ApplicationIdentityDbContext();
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));
            _roleManager = new RoleManager<ApplicationUserRole>(new RoleStore<ApplicationUserRole>(_context));
            _unitOfowrk = unitOfwork;
        }

        public async Task<IdentityResult> AddRole(ApplicationUserRole role)
        {
            return await _roleManager.CreateAsync(role);
        }

        public async Task<IdentityResult> EditRole(ApplicationUserRole role)
        {
            var currentUserRole = _roleManager.FindById(role.Id.ToString());
            currentUserRole.Name = role.Name;
            currentUserRole.LastAction = role.LastAction;
            currentUserRole.LastActionBy = role.LastActionBy;

            return await _roleManager.UpdateAsync(currentUserRole);
        }

        public async Task<ApplicationUser> FindUser(string userName, string password)
        {
            ApplicationUser user = await _userManager.FindAsync(userName, password);
            return user;
        }

        public async Task<ApplicationUser> FindUserById(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);
            return user;
        }

        public List<ApplicationUserRole> GetRoles()
        {
            IEnumerable<ApplicationUserRole> roles = _roleManager.Roles;
            return roles.ToList();
        }

        public List<ApplicationUser> GetUsers()
        {
            IEnumerable<ApplicationUser> users = _userManager.Users;
            return users.ToList();
        }

        public async Task<IdentityResult> RegisterUser(ApplicationUser user, string password)
        {
            try
            {
                return await _userManager.CreateAsync(user, password);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        public async Task<IdentityResult> UpdateUser(ApplicationUser user)
        {
            var currentUser = _userManager.FindById(user.Id.ToString());
            currentUser.UserName = user.UserName;
            currentUser.RoleId = user.RoleId;
            currentUser.CreatedBy = user.CreatedBy;
            currentUser.CreatedDate = user.CreatedDate;
            currentUser.LastAction = user.LastAction;
            currentUser.LastActionBy = user.LastActionBy;
            currentUser.PhoneNumber = user.PhoneNumber;
            currentUser.Email = user.Email;

            return await _userManager.UpdateAsync(currentUser);
        }
    }
}
