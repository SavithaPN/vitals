﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.CommonLogic.Interfaces;
using Vitals.DataAccess.Repository.Interfaces;
using Vitals.Domain.Entities;

namespace Vitals.CommonLogic.Services
{
    public class VitalsService : IVitalsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<VitalType> _vitalTypeRepository;
        public VitalsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _vitalTypeRepository = _unitOfWork.GetRepository<VitalType>();
        }

        public Task<IEnumerable<VitalType>> ListVitalTypes()
        {
            return _vitalTypeRepository.GetAsync();
        }
    }
}
