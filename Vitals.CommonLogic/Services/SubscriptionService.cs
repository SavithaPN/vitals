﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.CommonLogic.Interfaces;
using Vitals.DataAccess.Repository.Interfaces;
using Vitals.Domain.Entities;

namespace Vitals.CommonLogic.Services
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Subscription> _subscriptionRepository;
        public SubscriptionService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _subscriptionRepository = _unitOfWork.GetRepository<Subscription>();

        }

        public Task<IEnumerable<Subscription>> ListSubcriptions()
        {
            return _subscriptionRepository.GetAsync();
        }
    }
}
