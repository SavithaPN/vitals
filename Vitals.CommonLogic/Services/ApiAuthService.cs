﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;
using Vitals.CommonLogic.Interfaces;
using System.Web;
using System.Net.Http;
using System.Net;

namespace Vitals.CommonLogic.Services
{
    public class ApiAuthService : IApiAuthService
    {
        public AuthResponseDto Authenticate(AuthInformationDto authInfo)
        {
            AuthResponseDto oResponse = new AuthResponseDto();
            oResponse.AuthSuccess = true;
            string url = "http://" + HttpContext.Current.Request.Url.Authority;
            string token = GetToken(url, authInfo.UserName, authInfo.Password);
            if (token != "")
            {
                oResponse.Token = token;
            }
            else
            {
                oResponse.AuthSuccess = false;
                oResponse.ErrorMessage = "Invalid Username or Password";
                oResponse.Token = "";
            }

            return oResponse;
        }

        private static string GetToken(string url, string userName, string password)
        {
            var pairs = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>( "grant_type", "password" ),
                        new KeyValuePair<string, string>( "username", userName ),
                        new KeyValuePair<string, string> ( "Password", password )
                    };
            var content = new FormUrlEncodedContent(pairs);
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            using (var client = new HttpClient())
            {
                var response = client.PostAsync(url + "/Token", content).Result;
                return response.Content.ReadAsStringAsync().Result;
            }
        }
    }
}
