﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.Common.Dto.PagedList;
using Vitals.CommonLogic.Interfaces;
using Vitals.DataAccess.Repository.Interfaces;
using Vitals.Domain.Entities;

namespace Vitals.CommonLogic.Services
{
    public class MeasurementService : IMeasurementService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IRepository<Measurement> _measurementRepository;
        private IRepository<MeasurementType> _measurementTypeRepository;
        private IRepository<MeasurementValueType> _measurementValueTypeRepository;
        private IRepository<Subscription> _subscriptionRepository;
        private IRepository<PatientExternalId> _patientExternalIdRepository;

        public MeasurementService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _measurementRepository = _unitOfWork.GetRepository<Measurement>();
            _measurementTypeRepository = _unitOfWork.GetRepository<MeasurementType>();
            _measurementValueTypeRepository = _unitOfWork.GetRepository<MeasurementValueType>();
            _subscriptionRepository = _unitOfWork.GetRepository<Subscription>();
            _patientExternalIdRepository = _unitOfWork.GetRepository<PatientExternalId>();
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddMeasurement(Measurement measurement)
        {
            try
            {
                _measurementRepository.Insert(measurement);
                await _unitOfWork.SaveAsync();
                return new WorkResultDto<Guid, GeneralWorkStatus>(measurement.Id, GeneralWorkStatus.Success);

            }
            catch (Exception ex)
            {
                var result = new WorkResultDto<Guid, GeneralWorkStatus>(Guid.Empty, GeneralWorkStatus.ExecutionFailed);
                result.Errors.Add(ex.Message);
                return result;
            }
        }

        public async Task<IEnumerable<Measurement>> GetMeasurements(Guid patientId, Guid subscriptionId, DateTime from, DateTime to)
        {
            IEnumerable<Measurement> measurements = new List<Measurement>();
            var checkSubcription = await _patientExternalIdRepository.GetAsync(p => p.PatientId == patientId && p.SubscriptionId == subscriptionId);
            if (checkSubcription != null && checkSubcription.Count() > 0)
            {
                measurements = await _measurementRepository.GetAsync(m => m.PatientId == patientId);

                if (from != null && to != null)
                {
                    measurements = measurements.Where(m => m.MeasurementDateTime >= from && m.MeasurementDateTime <= to).ToList();
                }
            }

            return measurements;
        }

        public async Task<IEnumerable<MeasurementType>> GetMeasurementTypes()
        {
            var measurementTypes = await _measurementTypeRepository.GetAsync();
            return measurementTypes;
        }

        public async Task<IEnumerable<MeasurementValueType>> GetMeasurementValueTypes()
        {
            var measurementValueTypes = await _measurementValueTypeRepository.GetAsync();
            return measurementValueTypes;
        }

        public async Task<PagedListViewModel<Measurement>> GetPagedMeasurements(Guid patientId, Guid subscriptionId, DateTime from, DateTime to, int skip = -1, int count = -1)
        {
            IEnumerable<Measurement> measurements = new List<Measurement>();
            PagedListViewModel<Measurement> measurementsPL = new PagedListViewModel<Measurement>();
            var checkSubcription = await _patientExternalIdRepository.GetAsync(p => p.PatientId == patientId && p.SubscriptionId == subscriptionId);
            if (checkSubcription != null && checkSubcription.Count() > 0)
            {
                measurements = await _measurementRepository.GetAsync(m => m.PatientId == patientId);

                if (from != null && to != null)
                {
                    measurements = measurements.Where(m => m.MeasurementDateTime >= from && m.MeasurementDateTime <= to).OrderBy(m=>m.MeasurementDateTime).ToList();
                    if(measurements != null)
                    {
                        measurementsPL.TotalItemCount = measurements.Count();
                    }
                    else
                    {
                        measurementsPL.TotalItemCount = 0;
                    }
                  
                }

                if(skip > -1 && count >0)
                {
                    measurements = measurements.Skip(skip).Take(count);
                }

                if(measurements != null)
                {
                    measurementsPL.PageItems = measurements.ToList();
                }
                
            }

            return measurementsPL;
        }
    }
}
