﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vitals.Common.Dto
{
    public class UserRoleDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid CreatedBy { get; set; }
        public Guid LastActionBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastAction { get; set; }
    }
}
