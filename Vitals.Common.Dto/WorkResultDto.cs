﻿using System.Collections.Generic;

namespace Vitals.Common.Dto
{
    public class WorkResultDto<TResult, TStatus> where TStatus: struct
    {
        public TResult Result { get; set; }
        public TStatus Status { get; set; }
        public List<string> Errors { get; set; }

        public WorkResultDto(TResult result, TStatus status)
        {
            Result = result;
            Status = status;
        }

        public WorkResultDto()
        {

        }
    }
}
