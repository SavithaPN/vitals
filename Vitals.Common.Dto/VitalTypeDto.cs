﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vitals.Common.Dto
{
    public class VitalTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
