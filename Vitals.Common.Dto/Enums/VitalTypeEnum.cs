﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vitals.Common.Dto.Enums
{
    public enum VitalTypeEnum
    {
        BPSystolic= 1,
        BPDiastolic =2,
        BG = 3,
        Temperature = 4,
        Weight = 5,
        SPO2 = 6,
        Pulse = 7
    }
}
