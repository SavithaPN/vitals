﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vitals.Common.Dto.Enums
{
    public enum EnumMeasurementVitalType
    {
        Standard = 1,
        Systolic,
        Diastolic,
        BeforeMeal = 10,
        AfterMeal,
        AfterMidNightSnack
    }
}
