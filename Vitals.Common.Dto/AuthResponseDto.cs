﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vitals.Common.Dto
{
    public class AuthResponseDto
    {
        public string Token { get; set; }
        public bool AuthSuccess { get; set; }
        public string ErrorMessage { get; set; }
    }
}
