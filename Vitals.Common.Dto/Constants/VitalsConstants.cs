﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vitals.Common.Dto.Constants
{
    public static class VitalsConstants
    {
        public static  string HealtCareRpmId = "756a71e8-d745-4ff9-b5f6-7c320ee9cdb5";
        public static class DefaultMeasurementConstants
        {
            public const string DefaultBloodPressureId = "9734d2c7-fdae-4ef5-90f1-cb09accbfaea";
            public const string DefaultBloodGlucoseId_l = "b3101294-b71a-471e-978d-55b54336406b";
            public const string DefaultBloodGlucoseId_dl = "44e75e92-67c7-4a8d-8fd6-a426389e9cd6";
            public const string DefaultTemperatureId_C = "a8afae25-f67e-40b7-b8c2-6743611b1e1f";
            public const string DefaultTemperatureId_F = "d4a7bbce-9c8f-4fb8-819a-eb71dd4d4d60";
            public const string DefaultWeightId_kg = "1c6e2863-8cbe-456b-a944-40c8aa9eadae";
            public const string DefaultWeightId_lbs = "cf191549-61a8-4545-81a6-5f2583df2a08";
            public const string DefaultSpo2Id = "3f73178d-0c07-4979-bfb0-c0abeb8b9819";
            public const string DefaultHeartRateId = "d122c61c-e5dc-4ae9-aa6f-0683b702d13d";
            

            public const string BloodPressure = "BloodPressure";
            public const string BloodGlucose = "BloodGlucose";
            public const string Temperature = "Temperature";
            public const string Weight = "Weight";
            public const string SPO2 = "SPO2";
            public const string HeartRate = "HeartRate";

            public const string BloodPressureUnit = "mmHg";
            public const string BloodPressureMin = "40";
            public const string BloodPressureMax = "280";
            public const string BloodGlucoseUnitL = "mmol/L";
            public const string BloodGlucoseUnitDl = "mg/dl";
            public const string BloodGlucoseMinL = "2.4";
            public const string BloodGlucoseMaxL = "22.2";
            public const string BloodGlucoseMinDl = "50";
            public const string BloodGlucoseMaxDl = "400";
            public const string TemperatureUnitC = "°C";
            public const string TemperatureUnitF = "°F";
            public const string TemperatureMinC = "0";
            public const string TemperatureMaxC = "120";
            public const string TemperatureMinF = "0";
            public const string TemperatureMaxF = "48.8";
            public const string WeightUnitKg = "kg";
            public const string WeightUnitLbs = "lbs";
            public const string WeightMinKg = "4.5";
            public const string WeightMaxKg = "300";
            public const string WeightMinLbs = "10";
            public const string WeightMaxLbs = "500";
            public const string SPO2Unit = "%";
            public const string SPO2Min = "40";
            public const string SPO2Max = "100";
            public const string HeartRateUnit = "bpm";
            public const string HeartRateMin = "4";
            public const string HeartRateMax = "180";

        }
    }
}
