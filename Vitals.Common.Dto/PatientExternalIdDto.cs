﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vitals.Common.Dto
{
    public class PatientExternalIdDto
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid ExternalId { get; set; }
        public bool Active { get; set; }
        public Guid SubscriptionId { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime LastAction { get; set; }
        public Guid LastActionBy { get; set; }
    }
}
