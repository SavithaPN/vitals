﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vitals.Common.Dto.Measurement
{
    public class MeasurementDto
    {
        public Guid Id { get; set; }
        public Guid PatientId { get; set; }
        public Guid MeasurementTypeId { get; set; }
        public string MeasurementName { get; set; }
        public Guid LinkedMeasurementId { get; set; }
        public string MeasurementValueTypeId { get; set; }
        public string Value { get; set; }
        public DateTime MeasurementDateTime { get; set; }
        public DateTime ObserverdDateTime { get; set; }
        public bool IsManual { get; set; }
        public string TakenBy { get; set; }
        public string TakenAt { get; set; }
        public bool IsProcessed { get; set; }
        public string AlertType { get; set; }
        public string AlertString { get; set; }
        public DateTime Period { get; set; }
    }
}
