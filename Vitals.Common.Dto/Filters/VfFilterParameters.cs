﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vitals.Common.Dto.Filters
{
    public class VfFilterParameters
    {
        public Guid PatientId { get; set; }
        public Guid SubscriptionId { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public string SearchText { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }


    }
}
