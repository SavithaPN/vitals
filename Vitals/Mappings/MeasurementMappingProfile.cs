﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vitals.Common.Dto.Measurement;
using Vitals.Domain.Entities;

namespace Vitals.Mappings
{
    public class MeasurementMappingProfile : Profile
    {
        public MeasurementMappingProfile()
        {
            CreateMap<MeasurementDto, Measurement>()

                .ForMember(m => m.Patient, opt => opt.Ignore())
                .ForMember(m => m.MeasurementType, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<Measurement, MeasurementDto>()
                .ForMember(m=>m.MeasurementName, opt=>opt.Ignore())
                .AfterMap((s, d) =>
            {
                d.Period = s.MeasurementDateTime;
            });
        }
    }
}