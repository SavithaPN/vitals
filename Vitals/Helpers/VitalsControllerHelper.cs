﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.CommonLogic.Interfaces;
using Vitals.Helpers.Interfaces;

namespace Vitals.Helpers
{
    public class VitalsControllerHelper : IVitalsControllerHelper
    {
        IVitalsService _vitalsService;

        public VitalsControllerHelper(IVitalsService vitalsService)
        {
            _vitalsService = vitalsService;
        }

        public async Task<IEnumerable<VitalTypeDto>> ListVitalTypes()
        {
            var vitalTypes = await _vitalsService.ListVitalTypes();
            List<VitalTypeDto> vitalTypeDtos = new List<VitalTypeDto>();
            if(vitalTypes != null && vitalTypes.Count() > 0)
            {
                vitalTypeDtos = Mapper.Map<IEnumerable<VitalTypeDto>>(vitalTypes).ToList();
            }

            return vitalTypeDtos;
        }
    }
}