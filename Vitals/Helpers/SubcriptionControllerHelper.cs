﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Vitals.Common.Dto;
using Vitals.CommonLogic.Interfaces;
using Vitals.Helpers.Interfaces;

namespace Vitals.Helpers
{
    public class SubcriptionControllerHelper : ISubscriptionControllerHelper
    {
        ISubscriptionService _subcriptionService;
        public SubcriptionControllerHelper(ISubscriptionService subscriptionService)
        {
            _subcriptionService = subscriptionService;
        }

        public async Task<IEnumerable<SubscriptionDto>> ListSubcriptions()
        {
            var subscriptions = await _subcriptionService.ListSubcriptions();
            List<SubscriptionDto> subscriptionDtos = AutoMapper.Mapper.Map<IEnumerable<SubscriptionDto>>(subscriptions).ToList();
            return subscriptionDtos;
        }
    }
}