﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.Common.Dto.Filters;
using Vitals.Common.Dto.Measurement;
using Vitals.Common.Dto.PagedList;

namespace Vitals.Helpers.Interfaces
{
    public interface IMeasurementControllerHelper
    {
        Task<IEnumerable<MeasurementTypeDto>> GetMeasurementTypes();
        Task<IEnumerable<MeasurementValueTypeDto>> GetMeasurementValueTypes();
        Task<IEnumerable<MeasurementDto>> GetMeasurements(Guid patientId, Guid subscriptionId, DateTime from, DateTime to);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddMeasurement(MeasurementDto measurement);
        Task<GeneralWorkStatus> AddValuesToQueue(MeasurementDto measurementDto);
        Task<PagedListViewModel<MeasurementDto>> GetPagedMeasurements(VfFilterParameters filters);
    }
}
