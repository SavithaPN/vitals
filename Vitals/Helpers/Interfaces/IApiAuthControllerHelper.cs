﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;

namespace Vitals.Helpers.Interfaces
{
    public interface IApiAuthControllerHelper
    {
         AuthResponseDto Authenticate(AuthInformationDto user);
    }
}
