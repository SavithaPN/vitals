﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;

namespace Vitals.Helpers.Interfaces
{
    public interface IPatientControllerHelper
    {
        Task<IEnumerable<PatientDto>> ListPatientsBasedOnCustId(Guid customerId);
        Task<IEnumerable<PatientDto>> ListAllPatients();
        Task<PatientDto> GetPatientByExternalId(string sExternalId);
        Task<WorkResultDto<Guid, GeneralWorkStatus>> AddPatient(PatientDto patient);
    }
}
