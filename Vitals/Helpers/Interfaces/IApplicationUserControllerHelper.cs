﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;

namespace Vitals.Helpers.Interfaces
{
    public interface IApplicationUserControllerHelper
    {
        Task<IdentityResult> Create(UserDto oUser);
        Task<IdentityResult> EditUser(UserDto oUser);
        Task<UserDto> FindUserById(Guid userId);
        Task<UserDto> FindUserByName(string userName);
        Task<UserDto> FindUser(string userName, string password);
        Task<IdentityResult> AddRole(UserRoleDto userRoleDto);
        Task<IdentityResult> EditRole(UserRoleDto userRoleDto);
        List<UserRoleDto> GetRoles();
    }
}
