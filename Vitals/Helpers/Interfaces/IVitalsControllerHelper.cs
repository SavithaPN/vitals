﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;

namespace Vitals.Helpers.Interfaces
{
    public interface IVitalsControllerHelper
    {
        Task<IEnumerable<VitalTypeDto>> ListVitalTypes();
        //Task<WorkResultDto<Guid,GeneralWorkStatus>> AddVital
    }
}
