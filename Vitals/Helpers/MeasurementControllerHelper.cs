﻿using AutoMapper;
using Microsoft.Azure.Storage.Queue;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.Common.Dto.Filters;
using Vitals.Common.Dto.Measurement;
using Vitals.Common.Dto.PagedList;
using Vitals.CommonLogic.Interfaces;
using Vitals.Domain.Entities;
using Vitals.Helpers.Interfaces;
//using System.Web.Script.Serialization;

namespace Vitals.Helpers
{
    public class MeasurementControllerHelper : IMeasurementControllerHelper
    {
        private IMeasurementService _measurementSerice;
        

        public MeasurementControllerHelper(IMeasurementService measurementService)
        {
            _measurementSerice = measurementService;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddMeasurement(MeasurementDto measurementDto)
        {
            Measurement measurement = Mapper.Map<Measurement>(measurementDto);
            return await _measurementSerice.AddMeasurement(measurement);
        }

        public async Task<GeneralWorkStatus> AddValuesToQueue(MeasurementDto measurementDto)
        {
            try
            {
                string storageName = System.Configuration.ConfigurationManager.AppSettings["StorageAccountName"];
                string storageKey = ConfigurationManager.AppSettings["StorageAccountKey"];

                var storageAccount = new CloudStorageAccount(new StorageCredentials(storageName, storageKey), true);
                var client = storageAccount.CreateCloudQueueClient();
                var queue = client.GetQueueReference("measurement-values-queue");
                queue.CreateIfNotExists();
                var measurementData = new JavaScriptSerializer().Serialize(measurementDto); ;
                await queue.AddMessageAsync(new Microsoft.WindowsAzure.Storage.Queue.CloudQueueMessage(measurementData.ToString()));
                return GeneralWorkStatus.Success;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return GeneralWorkStatus.ExecutionFailed;
            }
        }

        public async  Task<IEnumerable<MeasurementDto>> GetMeasurements(Guid patientId, Guid subscriptionId, DateTime from, DateTime to)
        {
            var measurements = await _measurementSerice.GetMeasurements(patientId, subscriptionId, from, to);
            List<MeasurementDto> measurementDtos = new List<MeasurementDto>();
            measurementDtos = Mapper.Map<IEnumerable<MeasurementDto>>(measurements).ToList();
            return measurementDtos;
        }

        public async Task<IEnumerable<MeasurementTypeDto>> GetMeasurementTypes()
        {
            var measurementTypes = await _measurementSerice.GetMeasurementTypes();
            List<MeasurementTypeDto> measurementTypeDtos = new List<MeasurementTypeDto>();
            measurementTypeDtos = Mapper.Map<IEnumerable<MeasurementTypeDto>>(measurementTypes).ToList() ;
            return measurementTypeDtos;
        }

        public async Task<IEnumerable<MeasurementValueTypeDto>> GetMeasurementValueTypes()
        {
            var measurementTypes = await _measurementSerice.GetMeasurementValueTypes();
            List<MeasurementValueTypeDto> measurementValueTypeDtos = new List<MeasurementValueTypeDto>();
            measurementValueTypeDtos = Mapper.Map<IEnumerable<MeasurementValueTypeDto>>(measurementTypes).ToList();
            return measurementValueTypeDtos;
        }

        public async Task<PagedListViewModel<MeasurementDto>> GetPagedMeasurements(VfFilterParameters filters)
        {
            PagedListViewModel<MeasurementDto> measurementsDtoPl = new PagedListViewModel<MeasurementDto>();
            var measurements = await _measurementSerice.GetPagedMeasurements(filters.PatientId, filters.SubscriptionId, filters.fromDate, filters.toDate, filters.Skip, filters.Take);
            if(measurements != null)
            {
                measurementsDtoPl.TotalItemCount = measurements.TotalItemCount;
                measurementsDtoPl.PageItems = Mapper.Map<List<MeasurementDto>>(measurements.PageItems);
            }

            return measurementsDtoPl;
        }
    }
}