﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Vitals.Common.Dto;
using Vitals.CommonLogic.Interfaces;
using Vitals.Domain.Entities;
using Vitals.Helpers.Interfaces;

namespace Vitals.Helpers
{
    public class ApplicationUserControllerHelper : IApplicationUserControllerHelper
    {
        IApplicationUserService _userService;
        public ApplicationUserControllerHelper(IApplicationUserService userService)
        {
            _userService = userService;
        }

        public async Task<IdentityResult> AddRole(UserRoleDto userRoleDto)
        {
            try
            {
                ApplicationUserRole identityRole = new ApplicationUserRole()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = userRoleDto.Name,
                    CreatedDate = userRoleDto.CreatedDate,
                    CreatedBy = userRoleDto.CreatedBy,
                    LastAction = userRoleDto.LastAction,
                    LastActionBy = userRoleDto.LastActionBy
                };

                return await _userService.AddRole(identityRole);
            }
            catch (DbEntityValidationException ex)
            {
                var errors = ex.EntityValidationErrors
                    .Where(e => e.IsValid)
                    .SelectMany(e => e.ValidationErrors)
                    .Select(e => e.ErrorMessage)
                    .ToArray();

                return new IdentityResult(errors);
            }
        }

        public async Task<IdentityResult> Create(UserDto oUser)
        {
            try
            {
                ApplicationUser user = new ApplicationUser()
                {
                    Id = Guid.NewGuid().ToString(),
                    CreatedDate = DateTime.UtcNow,
                    LastAction = DateTime.UtcNow,
                    Email = oUser.Email,
                    UserName = oUser.UserName,
                    CreatedBy = oUser.CreatedBy,
                    LastActionBy = oUser.LastActionBy,
                    RoleId = oUser.RoleId,
                    Active = true
                };

                return await _userService.RegisterUser(user, oUser.Password);
            }
            catch (DbEntityValidationException ex)
            {
                var errors = ex.EntityValidationErrors
                             .Where(e => e.IsValid)
                             .SelectMany(e => e.ValidationErrors)
                             .Select(e => e.ErrorMessage)
                             .ToArray();
                return new IdentityResult(errors);
            }
        }

        public Task<IdentityResult> EditRole(UserRoleDto userRoleDto)
        {
            throw new NotImplementedException();
        }

        public async Task<IdentityResult> EditUser(UserDto oUser)
        {
            ApplicationUser user = Mapper.Map<ApplicationUser>(oUser);
            return await _userService.UpdateUser(user);
        }

        public async Task<UserDto> FindUser(string userName, string password)
        {
            var user = await _userService.FindUser(userName, password);
            UserDto userInfo = new UserDto
            {
                Id = Guid.Parse(user.Id),
                UserName = user.UserName,
                Email = user.Email,
                CreatedDate = user.CreatedDate,
                CreatedBy = user.CreatedBy,
                LastAction = user.LastAction,
                LastActionBy = user.LastActionBy,
                RoleId = user.RoleId
            };

            return userInfo;
        }

        public async Task<UserDto> FindUserById(Guid userId)
        {
            var user = await _userService.FindUserById(userId.ToString());
            UserDto userInfo = new UserDto
            {
                Id = Guid.Parse(user.Id),
                UserName = user.UserName,
                Email = user.Email,
                CreatedDate = user.CreatedDate,
                CreatedBy = user.CreatedBy,
                LastAction = user.LastAction,
                LastActionBy = user.LastActionBy,
                RoleId = user.RoleId
            };
            return userInfo;
        }

        public Task<UserDto> FindUserByName(string userName)
        {
            throw new NotImplementedException();
        }

        public List<UserRoleDto> GetRoles()
        {
            List<UserRoleDto> result = new List<UserRoleDto>();
            try
            {
                var roles = _userService.GetRoles();
                List<UserRoleDto> rolesDto = new List<UserRoleDto>();
                foreach (var role in roles)
                {
                    UserRoleDto roleDto = new UserRoleDto
                    {
                        Id = Guid.Parse(role.Id),
                        Name = role.Name,
                        CreatedDate = role.CreatedDate,
                        CreatedBy = role.CreatedBy,
                        LastAction = role.LastAction,
                        LastActionBy = role.LastActionBy
                    };
                    rolesDto.Add(roleDto);
                }

                
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message, ex.Source);
                return result;
            }
        }

        public IEnumerable<UserDto> ListUsers()
        {
            var users = _userService.GetUsers();
            List<UserDto> usersInfo = new List<UserDto>();
            foreach (var user in users)
            {
                UserDto userInfo = new UserDto
                {
                    UserName = user.UserName,
                    Email = user.Email,
                    Id = Guid.Parse(user.Id),
                    CreatedDate = user.CreatedDate,
                    CreatedBy = user.CreatedBy,
                    LastAction = user.LastAction,
                    LastActionBy = user.LastActionBy,
                    RoleId = user.RoleId
                };
                usersInfo.Add(userInfo);
            }

            return usersInfo;
        }
    }
}