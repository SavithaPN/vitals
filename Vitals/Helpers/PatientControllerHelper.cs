﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.CommonLogic.Interfaces;
using Vitals.Domain.Entities;
using Vitals.Helpers.Interfaces;

namespace Vitals.Helpers
{
    public class PatientControllerHelper : IPatientControllerHelper
    {
        private IPatientService _patientService;
        public PatientControllerHelper(IPatientService patientService)
        {
            _patientService = patientService;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddPatient(PatientDto patientDto)
        {
            var patient = Mapper.Map<Patient>(patientDto);
            WorkResultDto<Guid, GeneralWorkStatus> result = await _patientService.AddPatient(patient);
            return result;
        }

        public async Task<PatientDto> GetPatientByExternalId(string sExternalId)
        {
            var patient = await _patientService.GetPatientByExternalId(sExternalId);
            PatientDto patientDto = Mapper.Map<PatientDto>(patient);
            return patientDto;
        }

        public async Task<IEnumerable<PatientDto>> ListAllPatients()
        {
            var patients = await _patientService.ListAllPatients();
            List<PatientDto> patientDtos = Mapper.Map<IEnumerable<PatientDto>>(patients).ToList();
            return patientDtos;
        }

        public async Task<IEnumerable<PatientDto>> ListPatientsBasedOnCustId(Guid customerId)
        {
            var patients = await _patientService.ListPatientsBasedOnCustId(customerId);
            List<PatientDto> patientDtos = Mapper.Map<IEnumerable<PatientDto>>(patients).ToList();
            return patientDtos;
        }
    }
}