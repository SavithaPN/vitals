﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.CommonLogic.Interfaces;
using Vitals.Domain.Entities;
using Vitals.Helpers.Interfaces;

namespace Vitals.Helpers
{
    public class CustomerControllerHelper : ICustomerControllerHelper
    {
        ICustomerService _customerService;

        public CustomerControllerHelper(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddCustomer(CustomerDto customerDto)
        {
            Customer customer = Mapper.Map<Customer>(customerDto);
            return await _customerService.AddCustomer(customer);

        }

        public async Task<IEnumerable<CustomerDto>> ListCustomers()
        {
            var customers = await _customerService.ListCustomers();
            List<CustomerDto> customerDtos = Mapper.Map<IEnumerable<CustomerDto>>(customers).ToList();
            return customerDtos;
        }
    }
}