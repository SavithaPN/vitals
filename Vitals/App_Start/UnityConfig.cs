using System;
using System.Data.Entity;
using Unity;
using Unity.Injection;
using Vitals.CommonLogic.Interfaces;
using Vitals.CommonLogic.Services;
using Vitals.DataAccess.Contexts;
using Vitals.DataAccess.Repository;
using Vitals.DataAccess.Repository.Interfaces;
using Vitals.Helpers;
using Vitals.Helpers.Interfaces;

namespace Vitals
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();

            container.RegisterType<VitalsDBContext>(new InjectionConstructor("VitalsDBContext"));
            container.RegisterType<DbContext, VitalsDBContext>();
            container.RegisterType<IUnitOfWork, UnitOfWork>();

            container.RegisterType<IApiAuthService, ApiAuthService>();
            container.RegisterType<IApiAuthControllerHelper, ApiAuthControllerHelper>();

            container.RegisterType<IApplicationUserService, ApplicationUserService>();
            container.RegisterType<IApplicationUserControllerHelper, ApplicationUserControllerHelper>();

            container.RegisterType<ICustomerService, CustomerService>();
            container.RegisterType<ICustomerControllerHelper, CustomerControllerHelper>();

            container.RegisterType<IVitalsService, VitalsService>();
            container.RegisterType<IVitalsControllerHelper, VitalsControllerHelper>();

            container.RegisterType<ISubscriptionService, SubscriptionService>();
            container.RegisterType<ISubscriptionControllerHelper, SubcriptionControllerHelper>();

            container.RegisterType<IPatientService, PatientService>();
            container.RegisterType<IPatientControllerHelper, PatientControllerHelper>();

            container.RegisterType<IMeasurementService, MeasurementService>();
            container.RegisterType<IMeasurementControllerHelper, MeasurementControllerHelper>();
        }
    }
}