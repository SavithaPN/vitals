﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Vitals.Common.Dto;
using Vitals.Helpers.Interfaces;

namespace Vitals.Controllers
{
    [Authorize]
    public class VitalsController : ApiController
    {
        IVitalsControllerHelper _vitalsHelper;

        public VitalsController(IVitalsControllerHelper vitalsHelper)
        {
            _vitalsHelper = vitalsHelper;
        }

        [HttpGet]
        [Route("api/Vitals/GetVitalTypes")]
        public async Task<IEnumerable<VitalTypeDto>> ListVitalTypes()
        {
            return await _vitalsHelper.ListVitalTypes();
        }
        
    }
}