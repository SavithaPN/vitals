﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.Helpers.Interfaces;

namespace Vitals.Controllers
{
    [Authorize]
    public class PatientController : ApiController
    {
        IPatientControllerHelper _patientControllerHelper;

        public PatientController(IPatientControllerHelper patientControllerHelper)
        {
            _patientControllerHelper = patientControllerHelper;
        }

        [HttpGet]
        [Route("api/Patient/GetPatientByExternalId")]
        public async Task<PatientDto> GetPatientByExternalId(string externalId)
        {
            return await _patientControllerHelper.GetPatientByExternalId(externalId);
        }

        [HttpGet]
        [Route("api/Patient/ListAllPatients")]
        public async Task<IEnumerable<PatientDto>> ListAllPatients()
        {
            return await _patientControllerHelper.ListAllPatients();
        }

        [HttpGet]
        [Route("api/Patient/ListPatientsBasedOnCustId")]
        public async Task<IEnumerable<PatientDto>> ListPatientsBasedOnCustId(Guid customerId)
        {
            return await _patientControllerHelper.ListPatientsBasedOnCustId(customerId);
        }

        [HttpPost]
        [Route("api/Patient/AddPatient")]
        public async Task<WorkResultDto<Guid, GeneralWorkStatus>> AddPatient(PatientDto patientDto)
        {
            return await _patientControllerHelper.AddPatient(patientDto);
        }
    }
}