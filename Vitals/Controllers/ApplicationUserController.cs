﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Vitals.Common.Dto;
//using System.Web.Mvc;
using Vitals.Helpers.Interfaces;

namespace Vitals.Controllers
{
    [System.Web.Http.RoutePrefix("api/ApplicationUser")]
    public class ApplicationUserController : ApiController
    {
        private const string LocalLoginProvider = "Local";

        private IApplicationUserControllerHelper _userHelper;
        public ApplicationUserController(IApplicationUserControllerHelper userHelper)
        {
            _userHelper = userHelper;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IdentityResult> Register(UserDto user)
        {
            return await _userHelper.Create(user);
        }

        [HttpPost]
        [Route("UpdateUser")]
        public async Task<IdentityResult> UpdateUser(UserDto user)
        {
            return await _userHelper.EditUser(user);
        }

        [HttpGet]
        [Route("GetUserById")]
        public async Task<UserDto> FindUserById(Guid id)
        {
            return await _userHelper.FindUserById(id);
        }

        [HttpGet]
        [Route("GetUser")]
        public async Task<UserDto> FindUser(string userName, string password)
        {
            return await _userHelper.FindUser(userName, password);
        }

        //[HttpGet]
        //[Authorize]
        //[Route("ListUsers")]
        //public IEnumerable<UserDto> ListUsers()
        //{
        //    return _userHelper.ListUsers();
        //}

        [HttpPost]
        [Route("AddRole")]
        public async Task<IdentityResult> CreateRole(UserRoleDto roleDto)
        {
            return await _userHelper.AddRole(roleDto);
        }

        [HttpPost]
        [Route("EditRole")]
        public async Task<IdentityResult> EditRole(UserRoleDto roleDto)
        {
            return await _userHelper.EditRole(roleDto);
        }

        [HttpGet]
        [Route("GetRoles")]
        public List<UserRoleDto> ListRoles()
        {
            return _userHelper.GetRoles();
        }
    }
}