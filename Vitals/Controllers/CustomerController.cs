﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.Helpers.Interfaces;

namespace Vitals.Controllers
{
    [Authorize]
    public class CustomerController : ApiController
    {
        ICustomerControllerHelper _customerHelper;

        public CustomerController(ICustomerControllerHelper customerHelper)
        {
            _customerHelper = customerHelper;
        }

        [HttpGet]
        [Route("api/ListCustomers")]
        public async Task<IEnumerable<CustomerDto>> ListCustomers()
        {
            return await _customerHelper.ListCustomers();
        }

        [HttpPost]
        [Route("api/AddCustomer")]
        public async Task<WorkResultDto<Guid,GeneralWorkStatus>> AddCustomer(CustomerDto customerDto)
        {
            return await _customerHelper.AddCustomer(customerDto);
        }
    }
}