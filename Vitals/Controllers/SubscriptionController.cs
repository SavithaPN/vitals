﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using Vitals.Common.Dto;
using Vitals.Helpers.Interfaces;

namespace Vitals.Controllers
{
    [Authorize]
    public class SubscriptionController : ApiController
    {
        ISubscriptionControllerHelper _subscriptionHelper;

        public SubscriptionController(ISubscriptionControllerHelper subscriptionHelper)
        {
            _subscriptionHelper = subscriptionHelper;
        }

        [HttpGet]
        [Route("api/Subcription/ListSubscriptions")]
        public async Task<IEnumerable<SubscriptionDto>> ListSubscriptions()
        {
            return await _subscriptionHelper.ListSubcriptions();
        }
    }
}