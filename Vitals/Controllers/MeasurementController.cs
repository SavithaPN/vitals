﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Vitals.Common.Dto;
using Vitals.Common.Dto.Enums;
using Vitals.Common.Dto.Filters;
using Vitals.Common.Dto.Measurement;
using Vitals.Common.Dto.PagedList;
using Vitals.Helpers.Interfaces;

namespace Vitals.Controllers
{
    [Authorize]
    public class MeasurementController : ApiController
    {
        IMeasurementControllerHelper _measurementControllerHelper;

        public MeasurementController(IMeasurementControllerHelper measurementControllerHelper)
        {
            _measurementControllerHelper = measurementControllerHelper;
        }

        [HttpPost]
        [Route("api/Measurement/AddValuesToQueue")]
        public async Task<GeneralWorkStatus> AddValuesToQueue(MeasurementDto measurementDto)
        {
            return await _measurementControllerHelper.AddValuesToQueue(measurementDto);
        }


        [HttpGet]
        [Route("api/Measurement/GetMeasurements")]
        public async Task<IEnumerable<MeasurementDto>> GetMeasurements(Guid patientId, Guid subscriptionId, DateTime from, DateTime to)
        {
            return await _measurementControllerHelper.GetMeasurements(patientId, subscriptionId, from, to);
        }

        [HttpPost]
        [Route("api/Measurement/GetMeasurementsBasedOnPatient")]
        public async Task<IEnumerable<MeasurementDto>> GetMeasurementsBasedOnPatient(VfFilterParameters filters)
        {
            return await _measurementControllerHelper.GetMeasurements(filters.PatientId, filters.SubscriptionId, filters.fromDate, filters.toDate);
        }

        [HttpPost]
        [Route("api/Measurement/GetPagedMeasurements")]
        public async Task<PagedListViewModel<MeasurementDto>> GetPagedMeasurements(VfFilterParameters filters)
        {
            return await _measurementControllerHelper.GetPagedMeasurements(filters);
        }

        [HttpGet]
        [Route("api/Measurement/GetMeasurementTypes")]
        public async Task<IEnumerable<MeasurementTypeDto>> GetMeasurementTypes()
        {
            return await _measurementControllerHelper.GetMeasurementTypes();
        }

        [HttpGet]
        [Route("api/Measurement/GetMeasurementValueTypes")]
        public async Task<IEnumerable<MeasurementValueTypeDto>> GetMeasurementValueTypes()
        {
            return await _measurementControllerHelper.GetMeasurementValueTypes();
        }

        [HttpPost]
        [Route("api/Measurement/AddMeasurements")]
        public async Task<WorkResultDto<Guid,GeneralWorkStatus>> AddMeasurements(MeasurementDto measurementDto)
        {
            return await _measurementControllerHelper.AddMeasurement(measurementDto);
        }
    }
}