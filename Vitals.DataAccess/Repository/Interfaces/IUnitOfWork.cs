﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vitals.DataAccess.Repository.Interfaces
{
    public interface IUnitOfWork
    {
        void Dispose();
        IRepository<T> GetRepository<T>() where T : class, new();
        void Save();
        Task SaveAsync();
    }
}
