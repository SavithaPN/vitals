﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.DataAccess.Migrations;
using Vitals.Domain.Entities;

namespace Vitals.DataAccess.Contexts
{
    public class VitalsDBContext : DbContext
    {
        public VitalsDBContext(string conn) : base(conn)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<VitalsDBContext, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<VitalType> VitalTypes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<PatientExternalId> PatientExternalIds { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Measurement> Measurements { get; set; }
        public DbSet<MeasurementType> MeasurementTypes { get; set; }
        public DbSet<MeasurementValueType> MeasurementValueTypes { get; set; }
    }
}
