﻿namespace Vitals.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedMeasurementsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Measurements",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PatientId = c.Guid(nullable: false),
                        MeasurementTypeId = c.Guid(nullable: false),
                        LinkedMeasurementId = c.Guid(nullable: false),
                        MeasurementValueTypeId = c.String(),
                        Value = c.String(),
                        MeasurementDateTime = c.DateTime(nullable: false),
                        ObserverdDateTime = c.DateTime(nullable: false),
                        IsManual = c.Boolean(nullable: false),
                        TakenBy = c.String(),
                        TakenAt = c.String(),
                        IsProcessed = c.Boolean(nullable: false),
                        AlertType = c.String(),
                        AlertString = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MeasurementTypes", t => t.MeasurementTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.PatientId, cascadeDelete: true)
                .Index(t => t.PatientId)
                .Index(t => t.MeasurementTypeId);
            
            CreateTable(
                "dbo.MeasurementTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Unit = c.String(),
                        StartRange = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EndRange = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MeasurementValueTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Measurements", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.Measurements", "MeasurementTypeId", "dbo.MeasurementTypes");
            DropIndex("dbo.Measurements", new[] { "MeasurementTypeId" });
            DropIndex("dbo.Measurements", new[] { "PatientId" });
            DropTable("dbo.MeasurementValueTypes");
            DropTable("dbo.MeasurementTypes");
            DropTable("dbo.Measurements");
        }
    }
}
