﻿namespace Vitals.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedPatientExternalIdTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PatientExternalIds",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PatientId = c.Guid(nullable: false),
                        ExternalId = c.Guid(nullable: false),
                        Active = c.Boolean(nullable: false),
                        SubscriptionId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CustomerId = c.Guid(nullable: false),
                        ExternalId = c.Guid(nullable: false),
                        SubscriptionId = c.Guid(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        LastActionBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Customers", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Subscriptions", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Subscriptions", "CreatedBy", c => c.Guid(nullable: false));
            AddColumn("dbo.Subscriptions", "LastAction", c => c.DateTime(nullable: false));
            AddColumn("dbo.Subscriptions", "LastActionBy", c => c.Guid(nullable: false));
            DropColumn("dbo.Customers", "TimeZone");
            DropColumn("dbo.Customers", "CreateDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Customers", "CreateDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Customers", "TimeZone", c => c.Guid(nullable: false));
            DropColumn("dbo.Subscriptions", "LastActionBy");
            DropColumn("dbo.Subscriptions", "LastAction");
            DropColumn("dbo.Subscriptions", "CreatedBy");
            DropColumn("dbo.Subscriptions", "CreatedDate");
            DropColumn("dbo.Customers", "CreatedDate");
            DropTable("dbo.Patients");
            DropTable("dbo.PatientExternalIds");
        }
    }
}
