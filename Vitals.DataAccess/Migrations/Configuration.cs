﻿namespace Vitals.DataAccess.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Linq;
    using Vitals.Common.Dto.Constants;
    using Vitals.Common.Dto.Enums;
    using Vitals.DataAccess.Contexts;
    using Vitals.Domain.Entities;
    using static Vitals.Common.Dto.Constants.VitalsConstants;

    public sealed class Configuration : DbMigrationsConfiguration<VitalsDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        private string DefaultDateFormat = "MM-dd-yyyy HH:mm:ss";
        private string DefaultCountry = "USA";
        private Guid CreatedBy;
        private Guid LastActionBy;
        private Guid DefaultCustomerId = new Guid("193da4f9-e3cd-4ae0-9b1b-28232c163b62");
        protected override void Seed(Vitals.DataAccess.Contexts.VitalsDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            CreatedBy = Guid.NewGuid();
            LastActionBy = Guid.NewGuid();
            CreateRolesAndUsers(new ApplicationIdentityDbContext(), context);
            AddVitalTypes(context);
            AddDefaultCustomer(context);
            AddMeasurementTypes(context);
            AddHrpmSubscription(context);
        }

        private static void AddMeasurementType(VitalsDBContext context, string Id, string Name, string Unit, string dStartRange, string dEndRange)
        {
            MeasurementType oType = new MeasurementType
            {
                Id = Guid.Parse(Id),
                Name = Name,
                Unit = Unit,
                StartRange = decimal.Parse(dStartRange),
                EndRange = decimal.Parse(dEndRange)
            };
            context.MeasurementTypes.Add(oType);
        }

        private void AddMeasurementTypes(VitalsDBContext context)
        {
            if (context.MeasurementTypes != null && context.MeasurementTypes.Count() > 0)
            {
                return;
            }

            AddMeasurementType(context, DefaultMeasurementConstants.DefaultBloodPressureId,
               DefaultMeasurementConstants.BloodPressure, DefaultMeasurementConstants.BloodPressureUnit, DefaultMeasurementConstants.BloodPressureMin, DefaultMeasurementConstants.BloodPressureMax);

            AddMeasurementType(context, DefaultMeasurementConstants.DefaultBloodGlucoseId_l,
           DefaultMeasurementConstants.BloodGlucose, DefaultMeasurementConstants.BloodGlucoseUnitL, DefaultMeasurementConstants.BloodGlucoseMinL, DefaultMeasurementConstants.BloodGlucoseMaxL);

            AddMeasurementType(context, DefaultMeasurementConstants.DefaultBloodGlucoseId_dl, DefaultMeasurementConstants.BloodGlucose, DefaultMeasurementConstants.BloodGlucoseUnitDl,
            DefaultMeasurementConstants.BloodGlucoseMinDl, DefaultMeasurementConstants.BloodGlucoseMaxDl);

            AddMeasurementType(context, DefaultMeasurementConstants.DefaultTemperatureId_C, DefaultMeasurementConstants.Temperature, DefaultMeasurementConstants.TemperatureUnitC,
            DefaultMeasurementConstants.TemperatureMinC, DefaultMeasurementConstants.TemperatureMaxC);

            AddMeasurementType(context, DefaultMeasurementConstants.DefaultTemperatureId_F, DefaultMeasurementConstants.Temperature, DefaultMeasurementConstants.TemperatureUnitF,
                DefaultMeasurementConstants.TemperatureMinF, DefaultMeasurementConstants.TemperatureMaxF);

            AddMeasurementType(context, DefaultMeasurementConstants.DefaultWeightId_kg, DefaultMeasurementConstants.Weight, DefaultMeasurementConstants.WeightUnitKg,
                DefaultMeasurementConstants.WeightMinKg, DefaultMeasurementConstants.WeightMaxKg);

            AddMeasurementType(context, DefaultMeasurementConstants.DefaultWeightId_lbs, DefaultMeasurementConstants.Weight, DefaultMeasurementConstants.WeightUnitLbs,
                DefaultMeasurementConstants.WeightMinLbs, DefaultMeasurementConstants.WeightMaxLbs);

            AddMeasurementType(context, DefaultMeasurementConstants.DefaultSpo2Id, DefaultMeasurementConstants.SPO2, DefaultMeasurementConstants.SPO2Unit,
                DefaultMeasurementConstants.SPO2Min, DefaultMeasurementConstants.SPO2Max);

            AddMeasurementType(context, DefaultMeasurementConstants.DefaultHeartRateId, DefaultMeasurementConstants.HeartRate, DefaultMeasurementConstants.HeartRateUnit,
                DefaultMeasurementConstants.HeartRateMin, DefaultMeasurementConstants.HeartRateMax);

            //Now add Value Types

            AddMeasurementValueType(context, (int)EnumMeasurementVitalType.Standard, "Standard");
            AddMeasurementValueType(context, (int)EnumMeasurementVitalType.Systolic, "Systolic");
            AddMeasurementValueType(context, (int)EnumMeasurementVitalType.Diastolic, "Diastolic");
            AddMeasurementValueType(context, (int)EnumMeasurementVitalType.BeforeMeal, "Before Meal");
            AddMeasurementValueType(context, (int)EnumMeasurementVitalType.AfterMeal, "After Meal");
            AddMeasurementValueType(context, (int)EnumMeasurementVitalType.AfterMidNightSnack, "After Midnight Snack");
            context.SaveChanges();
        }

        private static void AddMeasurementValueType(VitalsDBContext context, int Id, string sName)
        {
            if (context.MeasurementValueTypes != null && context.MeasurementValueTypes.Count() <= 0)
            {
                MeasurementValueType oValueType = new MeasurementValueType
                {
                    Id = Id,
                    Name = sName
                };

                context.MeasurementValueTypes.Add(oValueType);
            }

        }

        private void AddDefaultCustomer(VitalsDBContext context)
        {
            Customer oCustomer = new Customer
            {
                Id = DefaultCustomerId
            };

            if (context.Customers.Where(p => p.Id == oCustomer.Id).Count() <= 0)
            {
                oCustomer.CustomerName = "Default";
                oCustomer.Group = "Default";
                oCustomer.Active = true;
                oCustomer.Country = DefaultCountry;
                oCustomer.DateTimeFormat = DefaultDateFormat;
                oCustomer.Address = "US KY";
                oCustomer.State = "Kentucky";
                oCustomer.CreatedDate = DateTime.UtcNow;
                oCustomer.CreatedBy = CreatedBy;
                oCustomer.LastAction = DateTime.UtcNow;
                oCustomer.LastActionBy = LastActionBy;
                context.Customers.Add(oCustomer);
                context.SaveChanges();
            }
        }

        private void AddVitalTypes(VitalsDBContext context)
        {
            if (context.VitalTypes != null && context.VitalTypes.Count() <= 0)
            {
                foreach (var item in Enum.GetValues(typeof(VitalTypeEnum)))
                {
                    VitalType vitalType = new VitalType();
                    vitalType.Id = Convert.ToInt32(item);
                    vitalType.Name = item.ToString();

                    context.VitalTypes.Add(vitalType);
                    context.SaveChanges();
                }
            }
        }

        private void CreateRolesAndUsers(ApplicationIdentityDbContext appContext, VitalsDBContext soContext)
        {
            var roleManager = new RoleManager<ApplicationUserRole>(new RoleStore<ApplicationUserRole>(appContext));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(appContext));

            if (!roleManager.RoleExists("Admin"))
            {
                //var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                var role = new ApplicationUserRole();
                role.Id = Guid.NewGuid().ToString();
                role.Name = "Admin";
                role.CreatedDate = DateTime.UtcNow;
                role.LastAction = DateTime.UtcNow;
                role.CreatedBy = CreatedBy;
                role.LastActionBy = LastActionBy;
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "admin";
                user.Email = "admin@brainmeshai.com";
                user.EmailConfirmed = true;
                user.CreatedDate = DateTime.UtcNow;
                user.LastAction = DateTime.UtcNow;
                user.RoleId = Guid.Parse(role.Id);
                user.Active = true;
                string adminPWD = "admin123";
                var adminUser = userManager.Create(user, adminPWD);

                if (adminUser.Succeeded)
                {
                    var result = userManager.AddToRole(user.Id, "Admin");
                }
            }
        }

        private void AddHrpmSubscription(VitalsDBContext vfContext)
        {
            List<Subscription> isHealthCareRpmPresent = null;
            
            if (vfContext.Subscriptions != null && vfContext.Subscriptions.Count() > 0)
            {
                isHealthCareRpmPresent = vfContext.Subscriptions.Where(s => s.AgentName.ToLower() == "healthcarerpm").ToList();
            }
            if ((vfContext.Subscriptions != null && vfContext.Subscriptions.Count() <= 0) || isHealthCareRpmPresent.Count()<=0)
            {
                Subscription hrpmSubscription = new Subscription();
                hrpmSubscription.Id = Guid.Parse(VitalsConstants.HealtCareRpmId);
                hrpmSubscription.CustomerId = DefaultCustomerId;
                hrpmSubscription.AgentName = "HealthcareRpm";
                hrpmSubscription.CreatedBy = Guid.NewGuid();
                hrpmSubscription.LastActionBy = Guid.NewGuid();
                hrpmSubscription.CreatedDate = DateTime.Now;
                hrpmSubscription.LastAction = DateTime.Now;

                vfContext.Subscriptions.Add(hrpmSubscription);
                vfContext.SaveChanges();
            }
        }
    }
}
