﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.DataAccess.Contexts;

namespace Vitals.DataAccess.Initializer
{
    public class MigrationContextFactory: IDbContextFactory<VitalsDBContext>
    {
        public VitalsDBContext Create()
        {
            return new VitalsDBContext("VitalsDBContext");
        }
    }
}
