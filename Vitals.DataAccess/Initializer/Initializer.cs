﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vitals.DataAccess.Contexts;
using Vitals.DataAccess.Migrations;

namespace Vitals.DataAccess.Initializer
{
    public class CustomInitializer : MigrateDatabaseToLatestVersion<VitalsDBContext, Configuration>
    {
        public override void InitializeDatabase(VitalsDBContext context)
        {
            base.InitializeDatabase(context);
        }

    }
}
